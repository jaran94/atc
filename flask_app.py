from flask import Flask, render_template, jsonify
import server
from datetime import datetime
app = Flask(__name__)
start_datetime = datetime.now()


class AirportFlaskServer:
    def __init__(self):
        self.shared_state = server.shared_state
        self.db_manager = server.global_db_manager

    def start_airport(self):
        if not self.shared_state.is_running():
            server.main(generate_plot=False, thread_join=False)
            self.shared_state.change_running_state()
            return jsonify({"message": "Application started."}), 200
        else:
            return jsonify({"message": "Application is already running."}), 200

    def pause_application(self):
        if self.shared_state.is_running() and not self.shared_state.is_paused():
            self.shared_state.change_pause_state()
            return jsonify({'message': 'Application paused.'}), 200
        else:
            return jsonify({'message': 'No running application to pause/Server already paused.'}), 200

    def resume_application(self):
        if self.shared_state.is_running() and self.shared_state.is_paused():
            self.shared_state.change_pause_state()
            return jsonify({'message': 'Application resumed.'}), 200
        else:
            return jsonify({'message': 'No paused application to resume.'}), 200

    def close_application(self):
        if self.shared_state.is_running():
            self.shared_state.change_running_state()
            return jsonify({'message': 'Application closed.'}), 200
        else:
            return jsonify({'message': 'No running application to close.'}), 200

    def return_query_result(self, sql_query: str):
        if self.shared_state.is_connected():
            result = self.db_manager.execution(sql_query)
            return jsonify(result), 200
        else:
            return jsonify({'message': 'No connection from client established.'}), 200

    def return_search_query_result(self, search_object, sql_query):
        if self.shared_state.is_connected():
            result = self.db_manager.execution(sql_query, (search_object,))
            if result is None:
                return jsonify({"error": "Airplane not found"}), 404
            else:
                return jsonify(result), 200
        else:
            return jsonify({'message': 'No connection from client established.'}), 200


airport_flask_server = AirportFlaskServer()


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/start-server")
def start_server():
    return airport_flask_server.start_airport()


@app.route("/stop-server")
def stop_server():
    return airport_flask_server.close_application()


@app.route("/pause-server")
def pause_server():
    return airport_flask_server.pause_application()


@app.route("/resume-server")
def resume_server():
    return airport_flask_server.resume_application()


@app.route("/uptime")
def uptime():
    delta = datetime.now() - start_datetime
    return jsonify({"message":f"uptime: {str(delta)}"}), 200


@app.route("/airplanes-count")
def airplanes_count():
    query = """
            SELECT COUNT(DISTINCT plane_id) AS distinct_planes
            FROM events
            WHERE run_id = (SELECT MAX(run_id) FROM events)
            AND event_type NOT IN ('crashed', 'landed')
            """
    result = airport_flask_server.return_query_result(query)
    return result


@app.route("/airplanes-ids")
def airplanes_ids():
    query = """
            SELECT DISTINCT plane_id
            FROM events
            WHERE run_id = (SELECT MAX(run_id) FROM events)
            AND event_type NOT IN ('crashed', 'landed')
            """
    result = airport_flask_server.return_query_result(query)
    return result


@app.route("/airplanes-crashes")
def airplanes_crashes():
    query = """
            SELECT *
            FROM events
            WHERE run_id = (SELECT MAX(run_id) FROM events)
            AND event_type = 'crashed'
            """
    result = airport_flask_server.return_query_result(query)
    return result


@app.route("/airplanes-landed")
def airplanes_landed():
    query = """
            SELECT *
            FROM events
            WHERE run_id = (SELECT MAX(run_id) FROM events)
            AND event_type = 'landed'
            """
    result = airport_flask_server.return_query_result(query)
    return result

@app.route("/airplanes/<int:id>")
def airplane_status(id):
    query = """
            SELECT *
            FROM live_data
            WHERE plane_id = ?
            """
    result = airport_flask_server.return_search_query_result(id, query)
    return result


if __name__ == "__main__":
    app.run(host="127.0.0.1")
