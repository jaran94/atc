import matplotlib.pyplot as plt
import numpy as np



class Plot:
    def __init__(self, planes_list: list, area_size: tuple, airport_pos: tuple):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, projection="3d")
        self.planes_list = planes_list
        self.x_size, self.y_size, self.z_size = area_size
        self.airport_pos = airport_pos

    def update_plot(self):
        self.ax.clear()
        self.ax.set_xlim3d([0, self.x_size])
        self.ax.set_ylim3d([0, self.y_size])
        self.ax.set_zlim3d([0, self.z_size])
        for airplane in self.planes_list:
            x, y, z = airplane.pos
            self.ax.scatter(x, y, z, c='r', marker="o")
        self.ax.scatter(*self.airport_pos, c="r", marker="s")
        plt.draw()

    def start_timer(self):
        timer = self.fig.canvas.new_timer(interval=1000)
        timer.add_callback(self.update_plot)
        timer.start()

    @staticmethod
    def plot_show():
        plt.show()
