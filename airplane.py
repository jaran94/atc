import threading

from airport import airspace
import random
import numpy as np


def random_init_wall():
    walls = ["north", "east", "south", "west"]
    choice = random.choice(walls)
    return choice


def random_init_pos(wall, area_size):
    choice = wall
    length, width, height = area_size
    match choice:
        case "north":
            x = random.randint(0, length)
            y = width
        case "east":
            x = length
            y = random.randint(0, width)
        case "south":
            x = random.randint(0, length)
            y = 0
        case "west":
            x = 0
            y = random.randint(0, width)
        case _:
            x = length
            y = width

    z = random.randint(int(0.4 * height), height)
    pos = (x, y, z)
    return pos


def random_init_heading(pos, area_size, simple=False):
    heading = None
    length, width, height = area_size
    x, y = pos[0], pos[1]
    if x != 0 and y == width:
        if simple:
            heading = "south"
        else:
            heading = random.randint(91, 269)
    elif x == length and y != 0:
        if simple:
            heading = "west"
        else:
            heading = random.randint(181, 359)
    elif x != 0 and y == 0:
        if simple:
            heading = "north"
        else:
            heading = random.choice((random.randint(0, 89), random.randint(271, 359)))
    elif x == 0 and y != 0:
        if simple:
            heading = "east"
        else:
            heading = random.randint(0, 179)
    return heading


class Airplane:
    _id_lock = threading.Lock()
    _id_counter = 0

    def __init__(self, controlled_airplane=False, area_size=(0, 0, 0)):
        self.controlled_airplane = controlled_airplane
        if self.controlled_airplane:
            self.pos = ()
            self.heading = 0
            self.h_speed = 0
            self.v_speed = 0
            self.target_pos = ()
            self.id = 0
        else:
            self.init_wall = random_init_wall()
            self.pos = random_init_pos(self.init_wall, area_size)
            self.heading = random_init_heading(self.pos, area_size, simple=True)
            self.h_speed = 5
            self.v_speed = -2
            self.max_v_speed = 10
            with Airplane._id_lock:
                self.id = Airplane._id_counter
                Airplane._id_counter += 1
            self.target_pos = ()
        self.crashed = False

    def return_dict(self):
        airplane_dict = {"id": self.id,
                         "data": {
                             "pos": self.pos,
                             "heading": self.heading,
                             "h_speed": self.h_speed,
                             "v_speed": self.v_speed,
                             "target_pos": self.target_pos
                         }}
        return airplane_dict

    def upload_data_dict(self, data_dict: dict):
        self.id = data_dict.get("id", "Airplane's ID not found")
        data = data_dict.get("data", "Data missing")
        self.pos = data.get("pos", "Data missing")
        self.heading = data.get("heading", "Data missing")
        self.h_speed = data.get("h_speed", "Data missing")
        self.v_speed = data.get("v_speed", "Data missing")
        self.target_pos = tuple(data.get("target_pos", "Data missing"))

    def change_heading(self, new_heading):
        self.heading = new_heading

    def move_simplified(self):
        match self.heading:
            case "north":
                delta_x = 0
                delta_y = self.h_speed
            case "east":
                delta_x = self.h_speed
                delta_y = 0
            case "south":
                delta_x = 0
                delta_y = -self.h_speed
            case "west":
                delta_x = -self.h_speed
                delta_y = 0
            case __:
                delta_x = 0
                delta_y = 0
        delta_z = self.v_speed
        delta = (delta_x, delta_y, delta_z)
        self.pos = tuple(x + y for x, y in zip(self.pos, delta))

    def move_towards_point(self, new_target_pos):
        if self.pos == new_target_pos:
            self.target_pos = ()
            print("Plane in target")
            return

        self._adjust_vertical_speed(new_target_pos)
        direction_hor = self._calculate_horizontal_direction(new_target_pos)
        direction = (*direction_hor, self.v_speed)
        new_pos = self._calculate_new_position(direction)
        self._is_approaching(new_pos, new_target_pos)
        self._is_valid_move(new_pos)

    def _calculate_horizontal_distance(self, new_target_pos):
        new_target_pos_hor = new_target_pos[:2]
        pos_hor = self.pos[:2]
        print(new_target_pos_hor, pos_hor)
        distance_hor = np.sqrt(np.sum((np.array(new_target_pos_hor) - np.array(pos_hor)) ** 2))
        return distance_hor

    def _adjust_vertical_speed(self, new_target_pos):
        distance_hor = self._calculate_horizontal_distance(new_target_pos)
        distance_ver = new_target_pos[2] - self.pos[2]
        if distance_hor < abs(distance_ver) and abs(self.v_speed) < self.max_v_speed:
            if distance_ver > 0:
                self.v_speed += 1
            else:
                self.v_speed -= 1
        print(f"Distance hor: {distance_hor}, distance ver: {distance_ver}")

    def _calculate_horizontal_direction(self, new_target_pos):
        new_target_pos_hor = new_target_pos[:2]
        pos_hor = self.pos[:2]
        direction_hor = tuple((np.array(new_target_pos_hor) - np.array(pos_hor)).clip(-self.h_speed, self.h_speed))
        print(f"Horizontal direction {direction_hor}")
        return direction_hor

    def _calculate_new_position(self, direction):
        new_pos = tuple(np.array(self.pos) + np.array(direction))
        new_pos = list(new_pos)
        for i in range(len(new_pos)):
            if new_pos[i] < 0:
                new_pos[i] = 0
        new_pos = tuple(int(x) for x in new_pos)
        return new_pos

    def _is_approaching(self, new_pos, new_target_pos):
            old_speed = self.v_speed
            condition_1 = new_pos[2] <= 50
            condition_2 = abs(new_pos[0] - new_target_pos[0]) > 50 or abs(new_pos[1] - new_target_pos[1]) > 50
            condition_3 = new_target_pos[2] == 0

            if all([condition_1, condition_2, condition_3]):
                self.v_speed = 0
            else:
                self.v_speed = old_speed

    def _is_valid_move(self, new_pos):
        if all(0 <= coord < size for coord, size in zip(new_pos, airspace.shape)):
            # Update the object's position
            self.pos = new_pos
            print("new cell set")
        else:
            # The new position is invalid, so the object cannot move there
            print("Error: Invalid move")

    def evasive_maneuver(self):
        self.target_pos = ()
        headings = ["north", "east", "south", "west"]
        previous_heading = self.heading
        new_heading = random.choice(headings)
        while new_heading == previous_heading:
            new_heading = random.choice(headings)
        self.heading = new_heading
        self.move_simplified()



