import logging
import socket
import time
import sys
import airplane
from connection import Connection
import threading
from concurrent.futures import ThreadPoolExecutor
import airport
import Database_management.db_management as db
from plot import Plot
from simulation_logic import logging_function, MultiThreadedClientService


class SharedState:
    def __init__(self):
        self._pause_state = 0
        self._running_state = 0
        self._connected_state = 0

    def change_pause_state(self):
        if self._pause_state:
            self._pause_state = 0
        else:
            self._pause_state = 1

    def change_running_state(self):
        if self._running_state:
            self._running_state = 0
        else:
            self._running_state = 1

    def change_connected_state(self):
        if self._connected_state:
            self._connected_state = 0
        else:
            self._connected_state = 1

    def is_paused(self):
        if self._pause_state:
            return True
        else:
            return False

    def is_running(self):
        if self._running_state:
            return True
        else:
            return False

    def is_connected(self):
        if self._connected_state:
            return True
        else:
            return False


global_db_manager = db.Sqlite()
shared_state = SharedState()


def start_multi_threaded_client_service(client_connection: socket.socket,
                                        tower: airport.TowerControl,
                                        airspace: airport.Airspace,
                                        server: Connection,
                                        database_manager: db.Sqlite):
    service = MultiThreadedClientService()
    service.start(airspace=airspace,
                  tower=tower,
                  client_connection=client_connection,
                  database_manager=database_manager,
                  server=server)
    service.run_service(shared_state)


def tower_thread_function(some_tower: airport.TowerControl,
                          server: Connection,
                          database_manager: db.Sqlite):
    logger_name = "Tower logger"
    tower_logger = logging_function(logger_name, "tower_logger.log", logging.DEBUG)
    tower_logger.debug(f"Database id: {database_manager}")
    while not some_tower.airplane_list and shared_state.is_running():
        if shared_state.is_paused():
            print("Tower process paused")
            time.sleep(1)
        else:
            tower_logger.info("Waiting for first plane.")
            time.sleep(1)
    while True:
        if shared_state.is_paused():
            print("Tower process paused")
            time.sleep(1)
        else:
            time.sleep(1)
            tower_logger.info("Looking for collisions")
            if not some_tower.airplane_list and shared_state.is_running():
                tower_logger.warning("All planes landed")
                server.activate_stop_trigger()
                break
            elif not some_tower.airplane_list and not shared_state.is_running():
                tower_logger.warning("Closing upon request.")
                server.activate_stop_trigger()
                break
            some_tower.update_corridors()
            some_tower.avoid_possible_flight_crash()
            if some_tower.planes_to_change_pos:
                tower_logger.warning(f"Possible collision detected! on planes: {some_tower.planes_to_change_pos}")
            for plane in some_tower.airplane_list:
                database_manager.update_live_data(plane.id, "in-air", plane.pos)

            check_collision = some_tower.check_for_collision()
            if check_collision:
                position, ids = check_collision
                for plane_id in ids:
                    for controlled_airplane in some_tower.airplane_list:
                        if controlled_airplane.id == plane_id:
                            database_manager.insert_event_data(plane_id, "crash", controlled_airplane.pos)
                            database_manager.update_live_data(plane_id, "crash", controlled_airplane.pos)
                            airplane.crashed = True

            tower_logger.info("Planes in airspace: %s", len(some_tower.airplane_list))
    if shared_state.is_running():
        shared_state.change_running_state()
        print(f"Tower changed shared_state to: {shared_state.is_running()}")


def submain(tower: airport.TowerControl, airspace: airport.Airspace, thread_pool):
    """This is actual main function, pyplot requirement, explanation in main"""
    main_logger = logging_function(__name__, "server.log", logging.WARNING)

    def log_exception(log_type, value, traceback):
        """Function to log all unhandled exceptions."""
        if issubclass(log_type, KeyboardInterrupt):
            sys.__excepthook__(log_type, value, traceback)
            return

        main_logger.exception(f"An unhandled exception occurred: {value}", exc_info=(log_type, value, traceback))

    sys.excepthook = log_exception

    server = Connection()
    server.socket.settimeout(60)
    server.start_connection()
    main_logger.info(f"Socket bound to {server.port}")
    main_logger.info("Socket is listening")

    database_manager = global_db_manager

    tower_thread = threading.Thread(target=tower_thread_function, args=(tower, server, database_manager))
    tower_thread.start()
    thread_count = 0
    while True:
        print(f"""Is running: {shared_state.is_running()}
        Is paused: {shared_state.is_paused()}
        Is connected: {shared_state.is_connected()}""")
        if not shared_state.is_running():
            print("Close on demand")
            break
        if shared_state.is_paused():
            print("Server paused")
            time.sleep(1)
        else:
            main_logger.debug(f"Stop trigger: {server.stop_trigger}")
            if server.stop_trigger:
                break
            try:
                client, address = server.socket.accept()
                client.settimeout(60)
            except socket.error as e:
                main_logger.error(e)
                print("TIME out in socket.accept try block")
                break
            main_logger.info(f"Connected to: {address[0]}:{str(address[1])}")
            thread_count += 1
            if thread_count == 1:
                shared_state.change_connected_state()
            thread_pool.submit(start_multi_threaded_client_service, client, tower, airspace, server, database_manager)
            main_logger.info(f"Thread number: {thread_count}")
    server.socket.close()
    database_manager.cancel_timer()
    print("SUBMAIN: SERVER CLOSING")


def main(generate_plot=False, thread_join=True):

    """ The main function calls submain_thread which is actually real main function, but it needed to be put in another
    thread due to pyplot requirement of execution itself in the main thread."""

    tower = airport.TowerControl()
    airspace = airport.Airspace()
    thread_pool = ThreadPoolExecutor(max_workers=110)
    submain_thread = threading.Thread(target=submain, args=(tower, airspace, thread_pool))
    submain_thread.start()
    if generate_plot:
        plot = Plot(planes_list=tower.airplane_list, area_size=airspace.area_size(), airport_pos=airspace.airport)
        plot.start_timer()
        plot.plot_show()
    """ Some kind of thread management is not allowing joining threads while run under Flask"""
    if thread_join:
        submain_thread.join()
    print("MAIN CLOSING")


if __name__ == "__main__":
    shared_state.change_running_state()
    main()
