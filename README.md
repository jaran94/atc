# ATC



## What is it? 

It's a simulation of ATC over an airport, where planes appears randomly into the controlled airspace. The task is to bring them all safely to the ground. 
It's done with a socket server and clients using multithreading. 

![atc_image](./images/atc.gif)
## Working details
Client_handler.py creates new thread with new connection and new airplane object. On server side, each accepted connection creates new "controlled_airplane" objects.
Both clients and server communicate with each other and exchange flight data (like position, speed, target, heading). 

The airport.py contains a Tower class which controls
and commands air the airplanes in the space. It checks for collisions, crossing paths for collisions - if that's detected 
the command for evasion maneuver is send to the particular airplane. 

The appearance of plane, landing or crash is stored in sqlite databases, so you can track it all.

All other actions are logged in files, so it can be reviewed later. 

And finally all of these data are showed in form of plot of matplotlib in real time (excluding refresh rate ;)) 

All of that can now be run in Flask app as a REST service. 

## How to run it? 

Without Flask: Clone repository, install libraries (matplotlib.plot and nummpy), run main then client_handler.

With Flask: Clone repository, install libraries (matplotlib.plot, numpy, Flask), run in terminal flask --app flask_app run, connect to localhost:5000 run server, run clienthandler.py. 

Soon I'm going to put it all in Dockerfiles along with Docker Compose running.



## Plans:

- Dockerize whole app
- creating more sophisticated (real one) algorithm for tower control
- it would be nice to have labels on plot alongside with path traces
- there are plenty options for sure to make it even more fun and interesting 