from sqlite3 import Error
import threading
from Database_management.connection_pool import ConnectionPool
from datetime import datetime


class Sqlite:
	def __init__(self, db_path="./Database_management/atc.db"):
		self.timer = None
		self.connection_pool = ConnectionPool(db_path=db_path)
		self._create_events_table_if_not_exists()
		self._drop_live_data_table()
		self._create_live_data_table_if_not_exists()
		self.repeat_clear()
		self.run_id = 0
		self._set_run_id()

	def repeat_clear(self):
		self.timer = threading.Timer(interval=60, function=self.repeat_clear)
		self.timer.start()
		self.connection_pool.clear_inactive_connections()

	def cancel_timer(self):
		self.timer.cancel()

	def _create_events_table_if_not_exists(self):
		query = """CREATE TABLE IF NOT EXISTS events (
		run_id integer NOT NULL, plane_id integer, event_type text, time text, x real, y real, z real
		)"""
		new_table = self.execution(query)
		self.connection_pool.logger.info(f"Table events and logger created: {new_table}")

	def _drop_live_data_table(self):
		query = """DROP TABLE IF EXISTS live_data"""
		drop_table = self.execution(query)
		self.connection_pool.logger.info(f"Table live_data dropped: {drop_table}")

	def _create_live_data_table_if_not_exists(self):
		query = """CREATE TABLE IF NOT EXISTS live_data (
		datetime text, plane_id integer, status text, x real, y real, z real
		)"""
		new_table = self.execution(query)
		self.connection_pool.logger.info(f"Table live_data created: {new_table}")

	def _set_run_id(self):
		query = """SELECT run_id FROM events ORDER BY run_id DESC LIMIT 1"""
		result = self.execution(query)
		print(f"Run_id result {result}")
		if result:
			self.run_id = result[0][0] + 1

	def execution(self, query, parameters=()):
		connection_pack = self.connection_pool.get_connection()
		self.connection_pool.logger.debug(f"Connection pack: {connection_pack}")
		connection = connection_pack[1]
		connection_id = connection_pack[0]

		try:
			cur = connection.execute(query, parameters)
			result = cur.fetchall()
			connection.commit()
			self.connection_pool.return_connection(connection_id)
			self.connection_pool.logger.info(f"Query result: {result}")
			return result
		except (ValueError, Error) as e:
			self.connection_pool.return_connection(connection_id, corrupted=True)
			self.connection_pool.logger.error(f"Error raised: {e}")

	def insert_event_data(self, plane_id, event_type, airplane_pos):
		query = """
		INSERT INTO events (run_id, plane_id, event_type, time, x, y, z)
		VALUES (?, ?, ?, ?, ?, ?, ?)
		"""
		parameters = (self.run_id, plane_id, event_type, datetime.now(), *airplane_pos)
		self.execution(query, parameters)

	def insert_live_data(self, plane_id, status, airplane_pos):
		query = """
		INSERT INTO live_data (datetime, plane_id, status, x, y, z)
		VALUES (?, ?, ?, ?, ?, ?)
		"""
		parameters = (datetime.now(), plane_id, status, *airplane_pos)
		self.execution(query, parameters)

	def update_live_data(self, plane_id, status, airplane_pos):
		query = """
		UPDATE live_data
		SET datetime = ?, status = ?, x = ?, y = ?, z = ?
		WHERE plane_id = ?
		"""
		parameters = (datetime.now(), status, *airplane_pos, plane_id)
		self.execution(query, parameters)