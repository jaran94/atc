import logging
import sqlite3

FLAG_FREE = 0
FLAG_OCCUPIED = 1
FLAG_CORRUPTED = 2


def logging_function(name, file_path, file_handler_level):
    log_format = "%(name)s %(asctime)s %(levelname)s %(message)s"
    logging.basicConfig(
        level=logging.DEBUG,
        format=log_format,
        datefmt="%Y-%m-%d %H:%M:%S"
    )
    main_logger = logging.getLogger(name)
    main_file_handler = logging.FileHandler(f"./logs/{file_path}", mode="w")
    formatter = logging.Formatter(log_format)
    main_file_handler.setFormatter(formatter)
    main_file_handler.setLevel(file_handler_level)
    main_logger.addHandler(main_file_handler)
    return main_logger


class ConnectionPool:

    def __init__(self, db_path="./Database_management/atc.db"):
        self.logger = logging_function("Database logger", "database.log", logging.INFO)
        self.db_path = db_path
        self.connections_list = []
        self.create_starting_pool(10)

    def create_starting_pool(self, conn_number):
        for x in range(conn_number):
            self.add_connections()
        self.logger.info(f"Created starting pool, connection list: {self.connections_list}")

    def add_connections(self):
        if len(self.connections_list) <= 90:
            conn = sqlite3.connect(self.db_path, check_same_thread=False)
            self.connections_list.append([conn, FLAG_FREE])
        else:
            self.logger.warning("Connections number limit has been reached.")

    def get_connection(self):
        for i, connection in enumerate(self.connections_list):
            if connection[1] == FLAG_FREE:
                # set the occupied flag
                self.connections_list[i][1] = FLAG_OCCUPIED
                return i, self.connections_list[i][0]
            elif connection[1] == FLAG_CORRUPTED:
                self.connections_list[i][0].close()
                self.connections_list.pop(i)
                if len(self.connections_list) < 10:
                    self.add_connections()
                self.logger.warning("Connection is corrupted")
        # These two lines will create connection if there isn't any free connection and call again
        self.logger.info("ADDING NEW CONNECTION AND GIVING AGAIN")
        self.logger.info(f"Connections before adding: {len(self.connections_list)}")
        self.add_connections()
        self.logger.info(f"Connections after adding: {len(self.connections_list)}")
        return self.get_connection()

    def return_connection(self, connection_id: int, corrupted=False):
        if not corrupted:
            self.connections_list[connection_id][1] = FLAG_FREE
        else:
            self.connections_list[connection_id][1] = FLAG_CORRUPTED

    def clear_inactive_connections(self):
        for i, connection in enumerate(self.connections_list[10:]):
            if connection[1] == FLAG_FREE or connection[1] == FLAG_CORRUPTED:
                self.connections_list[i][0].close()
                self.connections_list.pop(i)
        self.logger.info(f"Connection pool cleared. Number of connections: {len(self.connections_list)}")
