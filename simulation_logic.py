import socket
import threading
import time
import logging
import Database_management.db_management as db
import airplane
import airport
from connection import Connection, message_encode, message_decode
from airplane import Airplane
from enum import Enum


def logging_function(name, file_path, file_handler_level):
    log_format = "%(name)s %(asctime)s %(levelname)s %(message)s"
    logging.basicConfig(
        level=logging.DEBUG,
        format=log_format,
        datefmt="%Y-%m-%d %H:%M:%S"
    )
    main_logger = logging.getLogger(name)
    main_file_handler = logging.FileHandler(f"./logs/{file_path}", mode="w")
    formatter = logging.Formatter(log_format)
    main_file_handler.setFormatter(formatter)
    main_file_handler.setLevel(file_handler_level)
    main_logger.addHandler(main_file_handler)
    return main_logger


class Status(Enum):
    RUNNING = 0
    PAUSED = 1
    STOPPED = 2


class SimulationLogic:
    def __init__(self, logger: logging.Logger):
        self.airplane = None
        self.client = None
        self.area_size = None
        self.current_thread = None
        self.logger = logger
        self.connection_denied = False
        self.current_status = None

    def start(self):
        self.current_thread = threading.current_thread()
        self.client = Connection(is_server=False)
        self.logger.info(f"{self.current_thread} opening")
        self.client.start_connection()
        self.init_airplane()
        self.run_simulation()
        if self.current_status == Status.STOPPED:
            self.close_upon_request()
        else:
            self.close_connection()

    def init_airplane(self):
        self.current_status = Status.RUNNING
        data = self.receive_data()
        self.area_size = data.get("data").get("area_size")
        self.airplane = Airplane(area_size=self.area_size)
        airplane_data = self.airplane.return_dict()
        response = {"header": "init"}
        response.update(airplane_data)
        self.send_message(response)

    def run_simulation(self):
        while self.airplane.pos != airport.airport_pos:
            data = self.receive_data()
            self.process_message(data)
            if self.current_status == Status.PAUSED:
                time.sleep(1)
                continue
            time.sleep(1)
            if self.airplane.target_pos:
                self.airplane.move_towards_point(self.airplane.target_pos)
            else:
                self.logger.warning(f"ID: {self.airplane.id} don't have target.")
            if self.airplane.crashed:
                break
            elif self.connection_denied:
                break
            elif self.current_status == Status.STOPPED:
                break

    def receive_data(self):
        data = self.client.socket.recv(4096)
        incoming = message_decode(data)
        self.logger.debug(f"Incoming data {incoming}")
        return incoming

    def send_message(self, message):
        self.client.socket.sendall(message_encode(message))

    def process_message(self, data: dict):
        self.logger.debug(self.airplane.return_dict())
        try:
            header = data.get("header")
            match header:
                case "set_target_pos":
                    message = self.set_target_pos(data)
                    self.send_message(message)
                case "evasive_maneuver":
                    message = self.perform_evasive_maneuver()
                    self.send_message(message)
                case "crash":
                    self.report_crash()
                case "connection_denied":
                    self.handle_connection_denied()
                case "pause":
                    message = self.pause_simulation()
                    self.send_message(message)
                case "resume":
                    message = self.resume_simulation()
                    self.send_message(message)
                case "close":
                    self.current_status = Status.STOPPED
                case _:
                    message = self.status_update()
                    self.send_message(message)
        except AttributeError:
            message = "Empty message has been delivered."
            self.send_message(message)

    def set_target_pos(self, data):
        self.airplane.target_pos = data["data"].get("target_pos")
        print(self.airplane.target_pos)
        message = {"header": "changed_pos",
                   **self.airplane.return_dict(),
                   "message": f"Airplane id: {self.airplane.id} changed target position to {self.airplane.target_pos}"}
        return message

    def perform_evasive_maneuver(self):
        self.airplane.evasive_maneuver()
        message = {"header": "evasive_maneuver",
                   **self.airplane.return_dict(),
                   "message": f"Airplane id: {self.airplane.id} performed evasive maneuver."}
        return message

    def report_crash(self):
        self.logger.warning(f"Crash have been reported on position {self.airplane.pos}")
        self.airplane.crashed = True

    def handle_connection_denied(self):
        self.logger.info("Connection denied, probably due to reached connection limit number.")
        self.connection_denied = True

    def status_update(self):
        header = {"header": "status"}
        message = {**header, **self.airplane.return_dict()}
        return message

    def pause_simulation(self):
        if self.current_status == Status.RUNNING:
            self.current_status = Status.PAUSED
            message = {"header": "paused", "id": self.airplane.id}
        else:
            message = {**self.status_update(), "message": "Plane already paused."}
        return message

    def resume_simulation(self):
        if self.current_status == Status.PAUSED:
            self.current_status = Status.RUNNING
            message = {"header": "resumed", "id": self.airplane.id}
        else:
            message = {**self.status_update(), "message": "Plane already running."}
        return message

    def close_connection(self):
        if self.airplane.crashed:
            message = {"header": "crashed", "id": self.airplane.id}
        else:
            message = {"header": "stop", "id": self.airplane.id}
        self.logger.info(message)
        self.send_message(message)
        self.logger.info(f"{self.current_thread} closing.")
        self.client.socket.close()

    def close_upon_request(self):
        self.current_status = Status.STOPPED
        self.logger.info("Closing upon server request.")
        self.logger.info(f"{self.current_thread} closing.")
        message = {"header": "closing", "id": self.airplane.id}
        self.send_message(message)
        self.client.socket.close()


class MultiThreadedClientService:
    def __init__(self):
        self.controlled_airplane = None
        self.client_connection = None
        self.airspace = None
        self.tower = None
        self.database_manager = None
        self.server = None
        self.logger = None
        self.target = None
        self.database_manager = None
        self.local_state = None
        self.shared_state = None

    def start(self, airspace: airport.Airspace,
              tower: airport.TowerControl,
              client_connection: socket.socket,
              database_manager: db.Sqlite,
              server: Connection):
        self.airspace = airspace
        message = {"header": "init",
                   "info": "Server is working",
                   "data": {"area_size": airspace.area_size()}}
        self.client_connection = client_connection
        self.client_connection.sendall(message_encode(message))
        self.controlled_airplane = airplane.Airplane(controlled_airplane=True)
        self.tower = tower
        self.tower.add_airplane(self.controlled_airplane)
        self.target = airspace.airport
        self.database_manager = database_manager
        self.server = server

    def run_service(self, shared_state):
        self.shared_state = shared_state
        self.local_state = Status.RUNNING
        while True:
            if self.shared_state.is_paused():
                print(f"Airplane id: {self.controlled_airplane.id} process paused")
                time.sleep(0.5)
            else:
                message = self.receive_data()
                is_stopped = self.process_message(message)
                if is_stopped or self.local_state == Status.STOPPED:
                    break
                self.logger.info(f"Target pos: {self.controlled_airplane.target_pos}")
                command = self.give_commands()
                self.logger.debug(command)
                if not message:
                    break
                self.client_connection.sendall(message_encode(command))
        print(f"Planes before in tower: {len(self.tower.airplane_list)}")
        self.tower.remove_airplane(self.controlled_airplane)
        print(f"Planes after in tower: {len(self.tower.airplane_list)}")
        print(f"Plane {self.controlled_airplane.id} removed from tower list.")
        self.database_manager.insert_event_data(self.controlled_airplane.id, "plane removed",
                                                self.controlled_airplane.pos)
        self.client_connection.close()

    def receive_data(self):
        data = self.client_connection.recv(4096)
        message = message_decode(data)
        return message

    def process_message(self, data: dict):
        header = data.get("header")
        match header:
            case "init":
                self.initialize_airplane(data)
                return False
            case "status":
                self.update_status(data)
                return False
            case "stop":
                self.stop_service()
                return True
            case "changed_pos":
                self.changed_target_pos(data)
                return False
            case "crashed":
                self.logger.warning("Airplane crashed, connection closed by client")
                return True
            case "evasive_maneuver":
                self.evasive_maneuver_confirm(data)
                return False
            case "stop_all":
                self.stop_all_services()
                return True
            case "resumed":
                self.logger.info("Airplane %s has resumed", self.controlled_airplane.id)
                return False
            case "paused":
                self.logger.info("Airplane %s has paused.", self.controlled_airplane.id)
                return False
            case "closing":
                self.logger.info("Airplane %s is closing as requested.", self.controlled_airplane.id)
                return True
            case _:
                self.corrupted_data()
                return False

    def initialize_airplane(self, data):
        self.controlled_airplane.upload_data_dict(data)
        logger_name = f"Plane {self.controlled_airplane.id} log"
        self.logger = logging_function(logger_name, f"{self.controlled_airplane.id}.log", logging.WARNING)
        self.logger.debug(f"Database id: {self.database_manager}")
        self.database_manager.insert_event_data(self.controlled_airplane.id, "appearance", self.controlled_airplane.pos)
        self.database_manager.insert_live_data(self.controlled_airplane.id, "in-air`", self.controlled_airplane.pos)

    def update_status(self, data):
        self.controlled_airplane.upload_data_dict(data)
        airplane_data = self.controlled_airplane.return_dict()
        self.logger.debug(f"Server plane data: {airplane_data}")

    def stop_service(self):
        self.logger.warning("Connection closed by client.")
        self.database_manager.insert_event_data(self.controlled_airplane.id, "landing", self.controlled_airplane.pos)
        self.database_manager.update_live_data(self.controlled_airplane.id, "landed", self.controlled_airplane.pos)

    def changed_target_pos(self, data):
        self.controlled_airplane.upload_data_dict(data)
        self.logger.info("Target pos updated.")

    def evasive_maneuver_confirm(self, data):
        self.controlled_airplane.upload_data_dict(data)
        self.logger.warning("Plane performed evasive maneuver!")

    def stop_all_services(self):
        self.server.activate_stop_trigger()
        self.logger.warning("End of new communications")

    def corrupted_data(self):
        self.logger.error("Data corrupted")
        self.database_manager.insert_event_data(self.controlled_airplane.id, "error", self.controlled_airplane.pos)
        self.database_manager.update_live_data(self.controlled_airplane.id, "error", self.controlled_airplane.pos)

    def give_commands(self):
        if self.shared_state.is_paused() and self.local_state == Status.RUNNING:
            response = {"header": "pause"}
            self.logger.info("Airplane %s requested for pause.", self.controlled_airplane.id)
            self.local_state = Status.PAUSED
        elif not self.shared_state.is_paused() and self.local_state == Status.PAUSED:
            response = {"header": "resume"}
            self.logger.info("Airplane %s requested for resuming.", self.controlled_airplane.id)
            self.local_state = Status.RUNNING
        elif self.shared_state.is_paused() and self.local_state == Status.PAUSED:
            response = {"header": "misc", "message": "Pause still active."}
        elif not self.shared_state.is_running() and self.local_state == Status.RUNNING:
            self.logger.info("Connection for id %s is closing", self.controlled_airplane.id)
            self.local_state = Status.STOPPED
            response = {"header": "close"}
        elif self.controlled_airplane.id in self.tower.planes_to_change_pos:
            self.logger.warning(
                "Airplane %s on collision course, requesting evasive maneuver", self.controlled_airplane.id
            )
            response = self.tower.request_evasive_maneuver(self.controlled_airplane.id)
        elif self.controlled_airplane.crashed:
            response = {"header": "crash"}
        elif self.controlled_airplane.target_pos == () or self.controlled_airplane.target_pos == (0, 0, 0):
            response = self.tower.request_new_position(self.target, self.controlled_airplane.id)
            self.logger.info("%s requested for new position.", self.controlled_airplane.id)
        elif len(self.tower.airplane_list) > 100:
            response = {"header": "connection_denied"}
            self.logger.warning(f"Too many connections, connection {self.client_connection} denied.")
        else:
            response = {"header": "misc"}
            self.logger.info("%s keeps going.", self.controlled_airplane.id)
        return response

