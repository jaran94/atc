import threading

import numpy as np

LENGTH = 10000
WIDTH = 10000
HEIGHT = 5000
node_size = 10
nodes_len = int(LENGTH / node_size)
nodes_width = int(WIDTH / node_size)
nodes_height = int(HEIGHT / node_size)
airspace = np.zeros((nodes_len, nodes_width, nodes_height), dtype=object)
airport_pos = (int(nodes_len / 2), int(nodes_width / 2), 0)


class Airspace:
    def __init__(self, length=nodes_len, width=nodes_width, height=nodes_height):
        self.length = length
        self.width = width
        self.height = height
        self.airspace = airspace
        self.airport = airport_pos

    def area_size(self) -> tuple:
        return self.length, self.width, self.height


class TowerControl:
    def __init__(self):
        self.airplane_list = []
        self.corridors_list = []
        self.planes_to_change_pos = []
        self.planes_to_change_pos_lock = threading.Lock()
        self.airplane_list_lock = threading.Lock()
        self.corridors_list_lock = threading.Lock()
        # self.corridors_dict = {}

    def add_airplane(self, airplane):
        with self.airplane_list_lock:
            self.airplane_list.append(airplane)

    def remove_airplane(self, airplane):
        with self.airplane_list_lock:
            self.airplane_list.remove(airplane)

    def _clear_plane_from_changing_pos(self, id):
        with self.planes_to_change_pos_lock:
            self.planes_to_change_pos.remove(id)

    def create_corridors(self):
        for plane in self.airplane_list:
            corridor = []
            cell_pos = plane.pos
            plane_target = plane.target_pos
            if plane_target:
                while cell_pos != plane_target:
                    direction = tuple((np.array(plane_target) - np.array(cell_pos)).clip(-1, 1))
                    # Move the object one step in the chosen direction
                    new_pos = tuple(np.array(cell_pos) + np.array(direction))
                    cell_pos = new_pos
                    corridor.append(cell_pos)

                self.corridors_list.append({"id": plane.id, "corridor": corridor})

    def update_corridors(self):
        self.corridors_list.clear()
        self.create_corridors()

    def check_collision_course(self):
        matches = []
        for i, dict1 in enumerate(self.corridors_list):
            for j, dict2 in enumerate(self.corridors_list):
                if i != j:
                    for tuple1 in dict1["corridor"]:
                        for tuple2 in dict2["corridor"]:
                            if tuple1 == tuple2:
                                matches.append([dict1["id"], dict2["id"], tuple1])
        return matches

    def check_collision_timing(self):
        planes_for_course_correction = []
        collision_courses = self.check_collision_course()
        for match in collision_courses:
            id1 = match[0]
            id2 = match[1]
            collision_point = match[2]
            plane1_distance = 0
            plane2_distance = 0
            for plane in self.corridors_list:
                if plane["id"] == id1:
                    plane1_distance = plane["corridor"].index(collision_point)
                elif plane["id"] == id2:
                    plane2_distance = plane["corridor"].index(collision_point)
            if plane1_distance == plane2_distance:
                planes_for_course_correction.append(id1)
        return planes_for_course_correction

    def avoid_possible_flight_crash(self):
        planes_ids = self.check_collision_timing()
        self.planes_to_change_pos = list(set(planes_ids))

    def request_new_position(self, target_pos, id):
        for airplane in self.airplane_list:
            if airplane.id == id:
                data_dict = {"header": "set_target_pos",
                             "id": id,
                             "data": {
                                 "target_pos": target_pos
                             }}
                return data_dict

    def check_for_collision(self):
        position_dict = {}
        for airplane in self.airplane_list:
            if airplane.pos != airport_pos:
                if tuple(airplane.pos) in position_dict:
                    position_dict[tuple(airplane.pos)].append(airplane.id)
                else:
                    position_dict[tuple(airplane.pos)] = [airplane.id]

        for position, ids in position_dict.items():
            if len(ids) > 1:
                print(f"Position dictionary: {position_dict}")
                return position, ids
        return False

    def request_evasive_maneuver(self, id):
        for airplane in self.airplane_list:
            if airplane.id == id:
                data_dict = {"header": "evasive_maneuver",
                             "id": id,
                             "data": {
                                 "target_pos": None
                             }}
                self._clear_plane_from_changing_pos(id)
                return data_dict

    @staticmethod
    def report_crash():
        data_dict = {"header": "crash"}
        return data_dict
