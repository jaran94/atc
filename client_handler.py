import logging
import random
import time
import threading
from simulation_logic import SimulationLogic, logging_function


def new_connection(logger):
    client_simulator = SimulationLogic(logger=logger)
    client_simulator.start()


def main():
    logger = logging_function("client_log", "client.log", logging.INFO)
    threading_pool = []
    i = 0
    start_time = time.time()
    while time.time() - start_time < 300:
        name = f"Thread number: {i}"
        thread = threading.Thread(target=new_connection, name=name, args=(logger,))
        thread.start()
        threading_pool.append(thread)
        logger.info(f"Number of threads: {len(threading_pool)}")
        time.sleep(random.uniform(0.0, 10.0))
        i += 1


if __name__ == "__main__":
    main()
