import socket
import json
import threading


class Connection:
    def __init__(self, host="127.0.0.1", port=65432, is_server=True):
        self.host = host
        self.port = port
        self.is_server = is_server
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.stop_trigger = False
        self.stop_trigger_lock = threading.Lock()

    def start_connection(self):
        if self.is_server:
            try:
                self.socket.bind((self.host, self.port))
            except socket.error as e:
                print(str(e))
            self.socket.listen()
        else:
            try:
                self.socket.connect((self.host, self.port))
            except socket.error as e:
                print(str(e))

    def activate_stop_trigger(self):
        with self.stop_trigger_lock:
            self.stop_trigger = True
def message_decode(msg_bytes):
    try:
        message = json.loads(msg_bytes.decode("utf-8"))
        return message
    except ValueError:
        return "Decoding has failed."


def message_encode(msg):
    try:
        msg_bytes = (json.dumps(msg)).encode("utf-8")
        return msg_bytes
    except ValueError:
        return "Encoding has failed."


def check_data(decoded_msg: dict, key):
    if key in decoded_msg.keys():
        return True
    else:
        return False
