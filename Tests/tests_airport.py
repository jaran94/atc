import unittest
import airport
import airplane

tower = airport.TowerControl()
plane1 = airplane.Airplane(controlled_airplane=True, area_size=(200, 200, 100))
plane1.pos = (200, 100, 50)
plane1.heading = "west"
plane1.target_pos = (0, 100, 50)

plane2 = airplane.Airplane(controlled_airplane=True, area_size=(200, 200, 100))
plane2.pos = (0, 100, 50)
plane2.heading = "east"
plane2.target_pos = (200, 100, 50)
plane2.id = 1
tower.add_airplane(plane1)
tower.add_airplane(plane2)
tower.update_corridors()

class TestCrashRelated(unittest.TestCase):

    def setUp(self) -> None:

        print(f"Airplane list: {tower.airplane_list}")

        print(f"Corridor lists: {tower.corridors_list}")

    def tearDown(self) -> None:
        pass

    def test_check_collision_course(self):
        check = tower.check_collision_course()
        print(f"Check collision course: {check}")
        self.assertIsNotNone(check)

    def test_check_collision_timing(self):
        check = tower.check_collision_timing()
        print(f"Timing check: {check}")
        self.assertIsNotNone(check, "Check collision timing")

    def test_avoid_possible_flight_crash(self):
        tower.avoid_possible_flight_crash()
        print(f"List of planes to change position: {tower.planes_to_change_pos}")
        self.assertIsNotNone(tower.planes_to_change_pos, "Avoid test")



if __name__ == "__main__":
    unittest.main()
